# Traceroute Plus

Traceroute Plus is a traceroute utility I wrote for educational purposes in C&sharp;. Usage information can be found by calling it without any parameters: `tracerouteplus.exe`.

## Features
 - Configurable ping options:
	 - Timeout
	 - Time to wait between each request
	 - Number of retries for failed pings
	 - Maximum hop count (default: unlimited, tells you when the limit was hit)
 - Tells you what went wrong (DestinationNetworkUnreachable, TimedOut, etc.)
 - Optional DNS lookup for each host in traceroute (via `/dns` option)
 - Traceroute for multiple domains
	 - Read in domains from stdin
 - Bare output format for scripts
 - Guesses the round trip time for hosts that don't respond based on initial probe ping
	 - This is done by timing the amount of time the initial traceroute probe takes via System.Diagnotics.StopWatch
	 - This is usually out by at least ~10ms though I think because it recourds all the overhead of send the ping request as well

## Download
Cross-platform binaries are available via GitLab's automated build system, if you don't want to build it yourself. Simply visit the [pipelines page](https://gitlab.com/sbrl/TraceRoutePlus/pipelines), and select the appropriate download option:

![](https://i.imgur.com/9WtJcv3.png)

## Building
You can either build this with Visual Studio 2013, mono (gmcs I think?), or `csc.exe`.

### Visual Studio 2013
Open the project, and then go to `Build -> Build Solution`, or press `CTRL + SHIFT + B`.

### csc.exe
*Apparently you have to compile the `.resx` into a `.resources` file first before compiling it using a tool called `resgen`. If you know how to do that, please open an issue!`

Execute this in the `TraceRoutePlus` folder:
```
...\TraceRoutePlus> csc /out:tracerouteplus.exe /res:Properties\Resources.resources *.cs Properties\*.cs
```
_TODO: update these instructions_

## Examples
Example traceroute:
```bash
C:\>tracerouteplus github.com
Traceroute Plus
---------------
By Starbeamrainbowlabs <https://starbeamrainbowlabs.com>

=== github.com ===
 1: xxx.xxx.xxx.xxx 1ms
 2: xxx.xxx.xxx.xxx 33ms
 3: xxx.xxx.xxx.xxx 36ms
 4: xxx.xxx.xxx.xxx 54ms
 5: 4.69.149.18     119ms
 6: 4.53.116.102    115ms
 7: 192.30.252.207  118ms
 8: 192.30.252.130   118ms
=== github.com end ===
```
(Some ip addresses have been blanked out for security)

## Todo
 - Add option to specify starting hop
 - ...? (create an issue, or even better a pull request!)

## License
TraceRoutePlus is licensed under the **Mozilla Public License 2.0**. A Copy of this license is located in the LICENSE file in the root of this repository.
